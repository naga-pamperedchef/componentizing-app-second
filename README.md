## Install `@pc/lib` from the other repo

> Make sure `verdaccio` is running in your local and you are logged in.

> Also build and publish your library to local registry.

```
npm i @pc/lib --registry http://localhost:4873/
```